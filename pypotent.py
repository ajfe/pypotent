
class Git(object):
    pass # dummy class, prevent errors


# abstract class
class Requirement(object):

    def __init__(self, params):
        pass
    
    # abstract
    def fulfilled(self):
        pass

    # abstract
    def doItFfs(self):
        pass
    
    def makeitso(self):
        if not self.fulfilled():
            self.doItFfs()


class GitRepoExists(Requirement):
    
    def __init__(self, params):
        self.repoteam = params.repoteam
        self.reponame = params.reponame
        
    def fulfilled(self):
        gitrepo = Git.getRepo(self.repoteam, self.reponame)
        if gitrepo is not None:
            return True
        else:
            return False
        
    def doItFfs(self):
        Git.makeRepo(self.repoteam, self.reponame)
            
class TeamHasAccess(Requirement):
    def __init__(self, params):
        self.repoteam = params.repoteam
        self.reponame = params.reponame
        self.team = params.otherteam
        
    def fulfilled(self):
        gitrepo = Git.getRepo(self.repoteam, self.reponame)
        if self.team in gitrepo.accesslist:
            return True
        else:
            return False
        
    def doItFfs(self):
        Git.getRepo(self.repoteam, self.reponame).addAccess(self.team)
        
class GitRepoForked(Requirement):
    def __init__(self, params):
        self.repoteam_from = params.repoteam_from
        self.repoteam_to = params.repoteam_to
        self.reponame = params.reponame
        
    def fulfilled(self):
        repofrom = Git.getRepo(self.repoteam_from, self.reponame)
        repoto = Git.getRepo(self.repoteam_to, self.reponame)
        
        if repoto is not None and repoto.isFork(repofrom):
            return True
        else:
            return False
        
    def doItFfs(self):
        Git.getRepo(self.repoteam_from, self.reponame).fork(self.repoteam_to)
        
# etc.
class GitRepoHasBranch(Requirement):
    pass

class GitRepoHasNotBranch(Requirement):
    pass

class GitRepoHasWebhook(Requirement):
    pass


def magic():
    requirements= ()
    
    # our repo
    requirements.append(GitRepoExists({ "repoteam": "MyTeam", "reponame" : "super" }))
    requirements.append(TeamHasAccess({ "repoteam": "MyTeam", "reponame" : "super", "otherteam" : "OtherGuys" }))
    requirements.append(GitRepoHasBranch({ "repoteam": "MyTeam", "reponame" : "super", "branch" : "PREPROD" }))
    requirements.append(GitRepoHasBranch({ "repoteam": "MyTeam", "reponame" : "super", "branch" : "PROD" }))
    requirements.append(GitRepoHasWebhook({ "repoteam": "MyTeam", "reponame" : "super", "webhook" : "http://webhook.com/web/hook?project=super" }))
    
    # upstream repo
    requirements.append(GitRepoForked({ "repoteam_from": "MyTeam", "reponame" : "super", "repoteam_to" : "OtherGuys" }))
    requirements.append(GitRepoHasBranch({ "repoteam": "OtherGuys", "reponame" : "super", "branch" : "LIT" }))
    requirements.append(GitRepoHasBranch({ "repoteam": "OtherGuys", "reponame" : "super", "branch" : "DEV" }))
    requirements.append(GitRepoHasNotBranch({ "repoteam": "OtherGuys", "reponame" : "super", "branch" : "PREPROD" })) # cleanup fork
    requirements.append(GitRepoHasNotBranch({ "repoteam": "OtherGuys", "reponame" : "super", "branch" : "PROD" })) # cleanup fork
    requirements.append(TeamHasAccess({ "repoteam": "OtherGuys", "reponame" : "super", "otherteam" : "MyTeam" }))
    
    
    
    
    for req in requirements:
        req.makeitso()